package controlador;

import java.util.ArrayList;
import modelo.Algoritmo;
import modelo.Grafo;
import modelo.GrafoJson;

public class Controlador {
	private String jsonPath= Controlador.class.getResource("./local/grafo.json").getPath();
	private Grafo grafo;
	private String [][] matriz;
	
	
	public Controlador () {}
	
	public void crearGrafo(int cantidadVertices) {
		grafo = new Grafo(cantidadVertices);
	}
	
	public void agregarArista(int vertice1, int vertice2) {
		grafo.agregarArista(vertice1, vertice2);
	}
	
	public ArrayList<Integer> generarConjuntoMinimo(){	
			return Algoritmo.conjuntoDominanteMinimo(grafo);
	}

	public String [][] getMatriz(){
		matriz=new String [matrizTamanio()][matrizTamanio()];
		for(int i=0; i<matrizTamanio(); i++) {
			for(int j=0; j<matrizTamanio(); j++) {
				if(grafo.getMatriz()[i][j]) {
					matriz[i][j]="1";
				}else {
					matriz[i][j]="0";
				}
			}
		}
		return matriz;	
	}
	
	private int matrizTamanio() {
		return grafo.getMatriz().length;
	}
	
	public  void GuardarGrafo() {
		GrafoJson grafoJson = new GrafoJson(grafo.getVecinos());
		grafoJson.toJson(jsonPath);
	}
	
	public void cargarGrafo(){
		GrafoJson grafoJson = GrafoJson.fromJson(jsonPath);
		System.out.println("JSON Path:"+jsonPath);
		Grafo grafo = Grafo.fromVecinos(grafoJson.getVecinos());
		this.grafo = grafo;
	}
	
	public int cantidadMaximaDeAristas() {
		return this.grafo.getVecinos().size();
	}
	
}


