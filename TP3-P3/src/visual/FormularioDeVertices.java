package visual;

import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;

@SuppressWarnings("serial")
public class FormularioDeVertices extends JPanel {

	protected VerticeInput textFieldCantidadVertices;
	@SuppressWarnings("unused")
	private int cantidadVertices;
	private JLabel labelTitulo;
	
	/**
	 * Create the panel.
	 */
	public FormularioDeVertices() {
		setBounds(0, 0, 800, 400);
		setLayout(null);
		setOpaque(true);
		
		crearTitulo();
		generarTextFields();
	}
	
	private void crearTitulo() {
		labelTitulo = new JLabel("Crea tu grafo");
		labelTitulo.setHorizontalAlignment(SwingConstants.CENTER);
		labelTitulo.setFont(new Font("Arial Rounded MT Bold", Font.PLAIN, 22));
		labelTitulo.setBounds(147, 97, 454, 49);
		add(labelTitulo);
	}
	
	private void generarTextFields() {
		this.textFieldCantidadVertices= new VerticeInput("Cantidad de vertices:", 200);
		textFieldCantidadVertices.setLocation(89, 221);
		add(textFieldCantidadVertices);
		
	}
	
	public boolean verticesValidos() {
		return verificarCamposVertices();
	}
	
	private boolean  verificarCamposVertices() {
		return textFieldCantidadVertices.verificar();	
	}
}
