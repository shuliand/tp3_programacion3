package visual;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.SwingConstants;


@SuppressWarnings("serial")
public class ErrorLabel extends JLabel {
    public ErrorLabel()  {
        setFont(new Font("Arial", Font.PLAIN, 15));
        setHorizontalAlignment(SwingConstants.CENTER);
        setForeground(Color.red);
    }

    public void showError(String error){
       setText(error);
       
    }
    public void ocultarMensajeDeError() {
      setText("");
    }
}

