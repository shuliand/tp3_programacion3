package visual;

@SuppressWarnings("serial")
public class VerticeInput extends InputDeDato {

    /**
	 * 
	 */
	
	public VerticeInput(String labelText, int posicionY) {
        super(labelText, posicionY);
    }


    public boolean verificar(){
        try {
            verificarTextoVacio();
            int numero = convertirNumero();
            verificarNumeroVertice(numero);
        } catch (Exception e) {
            mostrarMensajeDeError(e.getMessage());
            return false;
           }
        return true;
    }
    private void verificarNumeroVertice(int numero) {
		if (numero==0 ) {
			throw new RuntimeException("El valor no puede ser cero");	
		}else if (numero<0){
			throw new RuntimeException("El valor no puede ser negativo");	
		}
	}
	
}
