package visual;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import java.awt.Font;

@SuppressWarnings("serial")
public class FormularioDeAristas extends JPanel {

	/**
	 * 
	 */
	
	protected AristaInput textFieldArista1;
	protected AristaInput textFieldArista2;
	private int cantidadVertices;
	private JLabel labelTitulo;
	
	/**
	 * Create the panel.
	 */
	public FormularioDeAristas(int cantidadVertices) {
		this.cantidadVertices=cantidadVertices;
		setBounds(0, 0, 800, 100);
		setLayout(null);
		setOpaque(true);
		crearTitulo();
		generarTextFields();	
	}
	
	private void crearTitulo() {
		labelTitulo = new JLabel("Ingrese arista entre dos vértices:");
		labelTitulo.setFont(new Font("Arial", Font.PLAIN, 20));
		labelTitulo.setHorizontalAlignment(SwingConstants.CENTER);
		labelTitulo.setBounds(10, 0, 770, 24);
		add(labelTitulo);
	}
	
	
	private void generarTextFields() {
		this.textFieldArista1= new AristaInput("Vértice", 200,cantidadVertices);
		textFieldArista1.setSize(340, 70);
		textFieldArista1.setLocation(20, 30);
		this.textFieldArista2= new AristaInput("Vértice", 300,cantidadVertices);
		textFieldArista2.setSize(340, 70);
		textFieldArista2.setLocation(394, 30);
		add(textFieldArista1);
		add(textFieldArista2);
	}
	
	public void limpiarTextFields() {
		textFieldArista1.setTextField("");
		textFieldArista2.setTextField("");
	}
	
	public boolean aristaValida() {
		return verificarCamposAristas();
	}
	
	private boolean verificarCamposAristas() {
		ocultarMensajesDeError();
		return  textFieldArista1.verificar() & textFieldArista2.verificar() & valoresDistintos(textFieldArista1,textFieldArista2) ;
	}
	
	private boolean valoresDistintos(InputDeDato vertice1, InputDeDato vertice2) {;
		try {
			int numero1 = vertice1.convertirNumero();
			int numero2 = vertice2.convertirNumero();

			if(numero1==numero2){
				throw new RuntimeException("Los valores no pueden ser iguales");
			}
		}catch (Exception e){
			vertice1.mostrarMensajeDeError(e.getMessage());
			vertice2.mostrarMensajeDeError(e.getMessage());
			return false;
		}
		return true;
	}
	
	 
	private void ocultarMensajesDeError() {
		textFieldArista1.ocultarMensajeDeError();
		textFieldArista2.ocultarMensajeDeError();
		repaint();
	}
}
