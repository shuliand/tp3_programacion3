package visual;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

@SuppressWarnings("serial")
public class MatrizGrafo extends JPanel {

	private String [][] matriz;
	private JPanel panelPrincipal;
	private JScrollPane scrollPane;
	
	/**
	 * Create the panel.
	 */
	public MatrizGrafo(String[][] matriz) {
		this.matriz= matriz;
		setBounds(0, 0, 385, 400);
		setBorder(LineBorder.createGrayLineBorder());
		setLayout(null);
		setOpaque(true);
		setBackground(Color.white);
		
		panelPrincipal = new JPanel();
		panelPrincipal.setBackground(new Color(255,255,255));
		panelPrincipal.setLayout(null);
		panelPrincipal.setPreferredSize(new Dimension((matrizTamanio()+2)*30,(matrizTamanio()+2)*30));
		
		scrollPane = new JScrollPane(panelPrincipal);
		scrollPane.setBounds(0, 30, 385, 370);
		add(scrollPane);
		
		JLabel lblNewLabel = new JLabel("Matriz de adyacencia");
		lblNewLabel.setFont(new Font("Arial", Font.PLAIN, 20));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(0, 0, 385, 30);
		add(lblNewLabel);
		
		generarMatriz();
		
	}
	
	protected void generarMatriz() {
		int y=30;
		int x=0;
		for(int i=0;i<matrizTamanio();i++) {
			for(int j=0;j<matrizTamanio();j++) {
				panelPrincipal.add(getLabel(matriz[i][j],x, y, 20, 20));
				x+=30;
			}
			y+=30;
			x=0;
		}
	}
	
	private int matrizTamanio() {
		return matriz.length;
	}
	
	private JLabel getLabel(String text, int x, int y, int h, int w) {
        JLabel label = new JLabel(text);
		if(text.equals("1")) {
			label.setForeground(Color.green);
		}
        label.setHorizontalAlignment(SwingConstants.CENTER);
        label.setBounds(x,y,h,w);
        return label;
    }
	
	protected void actualizarMatriz(int i, int j) {
		matriz[i-1][j-1]="1";
		matriz[j-1][i-1]="1";
		panelPrincipal.removeAll();
		revalidate();	
		repaint();
	}

}
