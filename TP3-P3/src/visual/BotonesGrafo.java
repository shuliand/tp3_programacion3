package visual;

import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class BotonesGrafo extends JPanel{
    protected JButton cargarMatriz;
    protected JButton crearMatriz;
    protected ErrorLabel mensajeDeError;
    
    public BotonesGrafo() {
        super();
        setLayout(new GridLayout(3,1));

        mensajeDeError  = new ErrorLabel();
        mensajeDeError.setVisible(true);
        this.crearMatriz =new  JButton("Crear grafo");
        this.cargarMatriz =new  JButton("Cargar grafo desde JSON");
        
        add(crearMatriz);
        add(mensajeDeError);
        add(cargarMatriz);

        setBounds(250, 400, 300, 100);

    }
    
    public void showError() {
        this.mensajeDeError.showError("No se ha podido encontrar el grafo");
        repaint();
    }
    public void ocultarError() {
        this.mensajeDeError.ocultarMensajeDeError();
    }
}

