package visual;

@SuppressWarnings("serial")
public class AristaInput extends InputDeDato {

	/**
	 * 
	 */
	
	private int valorMinimo;
    
    public AristaInput(String labelText, int posicionY,int valorMinimo) {
        super(labelText, posicionY);
        this.valorMinimo = valorMinimo;
    }
 
    public boolean verificar() {
       try {
        convertirNumero();
        verificarTextoVacio();
        verificarvalorMinimo();
       } catch (Exception e) {
        mostrarMensajeDeError(e.getMessage());
        return false;
       }
       return true;
    }
    
    private void verificarvalorMinimo() {
        int numero = convertirNumero();
        if((numero < 1 || numero > valorMinimo)) {
            throw new RuntimeException("El valor debe estar entre 1 y "+ valorMinimo  );
        }
    }

    public void setvalorMinimo(int valorMinimo) {
        this.valorMinimo = valorMinimo;
    }    
}

