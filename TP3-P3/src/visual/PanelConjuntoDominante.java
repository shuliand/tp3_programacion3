package visual;

import java.awt.Color;
import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import java.awt.Font;

@SuppressWarnings("serial")
public class PanelConjuntoDominante extends JPanel {

	private JPanel panelPrincipal;
	private JScrollPane scrollPane;
	private ArrayList<Integer> conjunto;
	

	/**
	 * Create the panel.
	 */
	public PanelConjuntoDominante(ArrayList<Integer> conjunto) {
		setBounds(0, 0, 385, 400);
		setBorder(LineBorder.createGrayLineBorder());
		setLayout(null);
		setOpaque(true);
		setBackground(Color.white);
		this.conjunto=conjunto;
		
		panelPrincipal = new JPanel();
		panelPrincipal.setBackground(new Color(255,255,255));
		panelPrincipal.setLayout(null);
		panelPrincipal.setPreferredSize(new Dimension((conjuntoTamanio()+2)*30,(conjuntoTamanio()+2)*30));
		
		scrollPane = new JScrollPane(panelPrincipal);
		scrollPane.setBounds(0, 30, 385, 370);
		add(scrollPane);
		
		JLabel lblNewLabel = new JLabel("Conjunto Dominante");
		lblNewLabel.setFont(new Font("Arial", Font.PLAIN, 20));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(0, 0, 385, 30);
		add(lblNewLabel);
		
		generarConjunto();	
	}
	
	private int conjuntoTamanio() {
		return conjunto.size();
	}
	
	private void generarConjunto() {
		int x=0;
		for (int i=0; i<conjuntoTamanio(); i++) {
			panelPrincipal.add(getLabel(""+(conjunto.get(i)+1),x,30,20,20));
			x+=30;
		}
		
	}
	
	
	private JLabel getLabel(String text, int x, int y, int h, int w) {
        JLabel label = new JLabel(text);
        label.setHorizontalAlignment(SwingConstants.CENTER);
        label.setBounds(x,y,h,w);
        return label;
    }
	
	protected void actualizarConjunto(ArrayList<Integer> nuevoConjunto) {
		this.conjunto=nuevoConjunto;
		panelPrincipal.removeAll();
		revalidate();	
		repaint();
		generarConjunto();
	}
}
