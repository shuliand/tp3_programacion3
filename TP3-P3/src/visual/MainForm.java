package visual;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import controlador.Controlador;


public class MainForm {
	private int cantidadVertices;
	private JFrame frame;
	private Controlador controlador;
	private JPanel interfazActual;
	private FormularioDeAristas formularioDeAristas;
	private FormularioDeVertices formularioDeVertices;
	private MatrizGrafo matrizGrafo;
	private PanelConjuntoDominante panelConjuntoDominante;
	private BotonesGrafo grafoInteracciones;
	private JButton buttonArista;
	private JButton buttonGuardarGrafo;

	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainForm window = new MainForm();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainForm() {
		controlador = new Controlador();
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 800, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setBackground(Color.white);
		frame.setLocationRelativeTo(null); 
		
		generarInterfazInicial();
	}
	
	private void generarInterfazInicial() {
		formularioDeVertices = new FormularioDeVertices();
		frame.getContentPane().add(formularioDeVertices);
		interfazActual=formularioDeVertices;
 
		generarGrafoInteracciones();
	}
	private void generarGrafoInteracciones() {
		this.grafoInteracciones = new BotonesGrafo();
		frame.getContentPane().add(grafoInteracciones );

		agregarInteracciones();
	}
	
	private void agregarInteracciones() {
		this.grafoInteracciones.cargarMatriz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {	
				try {
					cargarGrafo();
					onInteraccion();
				} catch (Exception ex) {
					System.out.println(ex.getMessage());
					grafoInteracciones.showError();
				}
			}
		});
		this.grafoInteracciones.crearMatriz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {	
				if(formularioDeVertices.verticesValidos()){
					crearGrafo();
					onInteraccion();
				   }		
			}
		});
	}

	private void onInteraccion() {
		frame.remove(grafoInteracciones);
		frame.remove(interfazActual);
		generarFormularioDeAristas();
		generarMatrizGrafo();
		generarPanelConjunto();		
		actualizar();
	}

	private void cargarGrafo() {
		controlador.cargarGrafo();
		cantidadVertices=controlador.cantidadMaximaDeAristas();
	}
	
	private void generarFormularioDeAristas() {
		formularioDeAristas= new FormularioDeAristas(cantidadVertices);
		frame.getContentPane().add(formularioDeAristas);
		interfazActual=formularioDeAristas;
		generarBotonArista();
		generarGuardarGrafo();
	}
	
	private void generarMatrizGrafo() {
		matrizGrafo = new MatrizGrafo(controlador.getMatriz());
		matrizGrafo.setBounds(6,100,385,400);
		frame.getContentPane().add(matrizGrafo);
	}
	
	private void generarBotonArista() {
		buttonArista = new JButton("Crear arista");
		buttonArista.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(formularioDeAristas.aristaValida()){
					agregarArista();
					matrizGrafo.actualizarMatriz(formularioDeAristas.textFieldArista1.convertirNumero(),
					formularioDeAristas.textFieldArista2.convertirNumero());
					panelConjuntoDominante.actualizarConjunto(controlador.generarConjuntoMinimo());
					formularioDeAristas.limpiarTextFields();
					matrizGrafo.generarMatriz();
				}
			}
		});
		buttonArista.setBounds(210, 510, 170, 23);
		frame.getContentPane().add(buttonArista);
	}
	private void generarGuardarGrafo() {
		buttonGuardarGrafo = new JButton("Guardar Grafo en JSON");
		buttonGuardarGrafo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controlador.GuardarGrafo();
			}
		});
		buttonGuardarGrafo.setBounds(400, 510, 170, 23);
		frame.getContentPane().add(buttonGuardarGrafo);
	}

	private void generarPanelConjunto() {
		panelConjuntoDominante= new PanelConjuntoDominante(controlador.generarConjuntoMinimo());
		panelConjuntoDominante.setBounds(394,100,385,400);
		frame.getContentPane().add(panelConjuntoDominante);
		actualizar();
	}
	
	private void actualizar() {
		frame.revalidate();	
		frame.repaint();
	}
	
	private void crearGrafo() {
		cantidadVertices=formularioDeVertices.textFieldCantidadVertices.convertirNumero();
		controlador.crearGrafo(cantidadVertices);
	}
	
	private void agregarArista() {
		int vertice1=formularioDeAristas.textFieldArista1.convertirNumero();
		int vertice2=formularioDeAristas.textFieldArista2.convertirNumero();
		controlador.agregarArista(vertice1-1, vertice2-1);
	}
	

}
 