package modelo;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;

import com.google.gson.Gson;

public class GrafoJson {

    private HashMap<Integer,ArrayList<Integer>> vecinos;

    public GrafoJson(HashMap<Integer,ArrayList<Integer>> vecinos) {
        this.vecinos = vecinos;
    }
    
    static public GrafoJson fromJson(String path) {
        Gson gson =  new Gson();
        GrafoJson ret = null;
        try {  
            FileReader reader = new FileReader(path);
            BufferedReader br = new BufferedReader(reader);
            ret = gson.fromJson(br,GrafoJson.class);
        } catch (Exception e) {
            // TODO: handle exception
        }
        return ret;
    }
    
    
    public void toJson(String path){
        Gson gson =  new Gson();
        String json = gson.toJson(this);
    
        try {
            FileWriter fWriter = new FileWriter(path);
            fWriter.write(json);
            fWriter.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public HashMap<Integer, ArrayList<Integer>> getVecinos() {
        return vecinos;
    }
}

