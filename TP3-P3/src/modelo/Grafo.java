package modelo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class Grafo  {
	
	private boolean[][] A;
	private int cantidadAristas;
	protected int cantidadVertices;
	private HashMap<Integer,ArrayList<Integer>> vecinos;
	
	public Grafo(int vertices) {
		this.cantidadVertices=vertices;
		this.cantidadAristas = 0;
		this.A = new boolean[vertices][vertices];
		this.vecinos=new HashMap<Integer,ArrayList<Integer>>(); 
		for (int i = 0; i < vertices; i++) {
			vecinos.put(i,new ArrayList<Integer>());
		}
	}
	 
	public void agregarArista(int i, int j) {
		verificarVertice(i);
		verificarVertice(j);
		verificarDistintos(i, j);
		this.vecinos.get(i).add(j);
		this.vecinos.get(j).add(i);
		A[i][j] = true;
		A[j][i] = true;
	}

	public void eliminarArista(int i, int j) {
		verificarVertice(i);
		verificarVertice(j);
		verificarDistintos(i, j);

		A[i][j] = false;
		A[j][i] = false;
	}

	public boolean existeArista(int i, int j) {
		verificarVertice(i);
		verificarVertice(j);
		verificarDistintos(i, j);

		return A[i][j];
	}

	public int tamano() {
		return A.length;
	}
	
	public int cantVecinos(int vertice) {
		int cantVecinos=0;
		for (int i=0; i<A[vertice].length; i++) {
			if(A[vertice][i]) {
				cantVecinos++;
			}
		}
		return cantVecinos;
	}
	
	public int getCantidadAristas() {
		for(int i=0;i<this.A.length;i++) {
			for(int j=0;j<this.A[0].length;j++) {
				if(A[i][j]) {
					cantidadAristas++;
				}
			}
		}
		return cantidadAristas/2;
	}
	
	public Set<Integer> vecinosHashSet(int i) {
		verificarVertice(i);
		Set<Integer> ret = new HashSet<Integer>();
		for(int j = 0; j < this.tamano(); ++j) if( i != j )
		{
			if( this.existeArista(i,j) )
				ret.add(j);
		}
		return ret;		
	}
	
	private void verificarVertice(int i) {
		if( i < 0 )
			throw new IllegalArgumentException("El vertice no puede ser negativo: " + i);
		
		if( i >= A.length )
			throw new IllegalArgumentException("Los vertices deben estar entre 0 y |V|-1: " + i);
	}

	private void verificarDistintos(int i, int j) {
		if( i == j )
			throw new IllegalArgumentException("No se permiten loops: (" + i + ", " + j + ")");
	}
	
	public boolean[][] getMatriz(){
		return A;
	}
	
	protected int cantidadVertices() {
		return cantidadVertices;
	}
	
	public  HashMap<Integer, ArrayList<Integer>> getVecinos() {
		return this.vecinos;
	}

	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for(int i=0;i<this.A.length;i++) { //fila
			for(int j=0;j<this.A[0].length;j++) {//columna
				if(A[i][j]) {
					sb.append("1  ");
				}else { 
					sb.append("0  ");
				}			
			}
			sb.append("\n");
		}
		return sb.toString();
	}
	

	public static Grafo fromVecinos(HashMap<Integer,ArrayList<Integer>> vecinos) {
		Grafo grafo = new Grafo(vecinos.size());

		Iterator<Integer> vertices = vecinos.keySet().iterator();

		while (vertices.hasNext()) {
			int vertice = vertices.next();
			ArrayList<Integer> vecinosDeVertice= vecinos.get(vertice);
			for (Integer vecino : vecinosDeVertice) {
				grafo.agregarArista(vertice,vecino);
			}
		}
		return grafo;
	}
	
}