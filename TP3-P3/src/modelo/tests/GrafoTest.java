package modelo.tests;

import static org.junit.Assert.*;
import org.junit.Test;
import modelo.Grafo;


public class GrafoTest {

	@Test (expected=IllegalArgumentException.class)
	public void agregarAristaVerticeIgual()
	{
		Grafo g = new Grafo(5);
		g.agregarArista(0,0);
	}

	@Test (expected=IllegalArgumentException.class)
	public void agregarAristaFueraDeRangoSuperior()
	{
		Grafo g = new Grafo(5);
		g.agregarArista(5,0);
	}

	
	@Test (expected=IllegalArgumentException.class)
	public void agregarAristaNegativa()
	{
		Grafo g = new Grafo(5);
		g.agregarArista(-1,0);
	}


	@Test
	public void agregarAristaValidaTest() 
	{
		Grafo g = new Grafo(5); 
		g.agregarArista(0, 1); 
		assertTrue(g.existeArista(0, 1)); 
	}
	
	@Test
	public void agregarAristaSimetriacaTest() 
	{
		Grafo g = new Grafo(5); 
		g.agregarArista(0, 1); 
		assertTrue(g.existeArista(1, 0)); 
	}
	
	@Test
	public void eliminarAristaTest() 
	{
		Grafo g = new Grafo(5);
		g.agregarArista(0, 1);
		g.eliminarArista(0, 1);
		assertFalse(g.existeArista(0, 1));
	}
	

}
