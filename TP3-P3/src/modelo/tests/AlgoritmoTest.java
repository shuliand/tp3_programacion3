package modelo.tests;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;
import modelo.Algoritmo;
import modelo.Grafo;

public class AlgoritmoTest {

	@Test(expected=IllegalArgumentException.class)
	public void algoritmoNullTest() {
		Grafo grafo=null;
		
		Algoritmo.conjuntoDominanteMinimo(grafo);		
	}
	
	@Test
	public void unVerticetest() {
		Grafo grafo=new Grafo(1);
		
		ArrayList<Integer>conjunto=Algoritmo.conjuntoDominanteMinimo(grafo);
		ArrayList<Integer>conjuntoEsperado=new ArrayList<>();
		conjuntoEsperado.add(0);
		
		Assert.assertEquals(conjuntoEsperado, conjunto);
	}
	
	@Test
	public void algoritmoValidoTest() {
		Grafo grafo=new Grafo(5);
		grafo.agregarArista(0,1);
		grafo.agregarArista(0,2);
		grafo.agregarArista(1,2);
		grafo.agregarArista(1,3);
		grafo.agregarArista(2,3);
		grafo.agregarArista(3,4);
		
		ArrayList<Integer>conjunto=Algoritmo.conjuntoDominanteMinimo(grafo);
		ArrayList<Integer>conjuntoEsperado=new ArrayList<>();
		conjuntoEsperado.add(1);
		conjuntoEsperado.add(3);
		
		Assert.assertEquals(conjuntoEsperado, conjunto);
	}
	
	@Test
	public void test() {
		Grafo grafo=new Grafo(2);
		grafo.agregarArista(1,0);
		
		ArrayList<Integer>conjunto=Algoritmo.conjuntoDominanteMinimo(grafo);
		ArrayList<Integer>conjuntoEsperado=new ArrayList<>();
		conjuntoEsperado.add(0);
		
		Assert.assertEquals(conjuntoEsperado, conjunto);
	}
	
}

