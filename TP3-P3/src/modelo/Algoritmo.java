package modelo;

import java.util.ArrayList;

public class Algoritmo {
	private static ArrayList<Integer> verticesRecorridos;
	private static ArrayList<Integer> conjuntoActual;
	private static ArrayList<Integer> conjuntoDominante;

	public static ArrayList<Integer> conjuntoDominanteMinimo(Grafo grafo) {
		if(grafo==null) {
			throw new IllegalArgumentException("El grafo no puede ser null");
		}
		verticesRecorridos=new ArrayList<>();
		conjuntoActual=new ArrayList<>();
		conjuntoDominante= new ArrayList<>();
		
		while(!encontroConjuntoDom(grafo.cantidadVertices())) {
			agregarVerticeConMasVecinos(grafo);		
		}
		return conjuntoDominante;	
	}

	private static boolean encontroConjuntoDom(int vertices) {
		for(int i=0;i<vertices;i++) {
			if(!conjuntoActual.contains(i)) {
				return false;
			}
		}		
		return true;
	}
		
	private static void agregarVerticeConMasVecinos(Grafo grafo) {
		int cantVecinosActual=0;
		int verticeConMasVecinos=0;
		
		for(int i=0;i<grafo.cantidadVertices();i++) {
			int cantVecinosVerticeActual=grafo.getVecinos().get(i).size();
			if(cantVecinosVerticeActual>cantVecinosActual && !verticesRecorridos.contains(i)) {
				cantVecinosActual=cantVecinosVerticeActual;
				verticeConMasVecinos=i;				
			}else if(cantVecinosVerticeActual==0 && !verticesRecorridos.contains(i)) {
				verticesRecorridos.add(i);
				conjuntoActual.add(i);
				conjuntoDominante.add(i);
			}
		}
		
		agregarVecinosNoRepetidos(grafo.getVecinos().get(verticeConMasVecinos),verticeConMasVecinos);	
		verticesRecorridos.add(verticeConMasVecinos);
		conjuntoActual.add(verticeConMasVecinos);
	}

	private static void agregarVecinosNoRepetidos(ArrayList<Integer> vecinos,int verticeConMasVecinos) {
		boolean agregoVerticeNuevo=false;
		
		for(int vecinoActual:vecinos) {
			if(!conjuntoActual.contains(vecinoActual)) {
				conjuntoActual.add(vecinoActual);
				agregoVerticeNuevo=true;
			}
		}
		
		if(agregoVerticeNuevo) {
			conjuntoDominante.add(verticeConMasVecinos);			
		}
	}

}
